from ResultBackEnd import resultlogic, USNValidate , Last4
from flask import Flask, request, flash, url_for, redirect, render_template, abort
import json

app=Flask(__name__)
app.config.from_pyfile('AppConfig.cfg')
app.config['PROPAGATE_EXCEPTIONS'] = True

@app.route('/result',methods=['GET', 'POST'])
def result():
    return render_template('result.html')

@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def index():
    error = None
    if request.method == 'POST':
        U = request.form['USN']
        u1 = USNValidate.USN(U)
        if u1.isValidUSN() == False:
            error = u1.getErrors()
            flash (error)
        else:
            marks,usn_name = resultlogic.get_result(U)
            sub=marks[0]
            subjects_vtu = []
            marks_ext = []
            marks_int = []
            total_sub = []
            for i in range(0,len(marks)):
                #print "Subject \n"
                subjects_vtu.append(marks[i][0])
                #print "Marks \n"
                marks_ext.append(int(marks[i][1]))
                marks_int.append(int(marks[i][2]))
                total_sub.append(marks_int[i]+marks_ext[i])
            int_sum_per = (sum(x for x in marks_int))
            ext_sum_per = (sum(x for x in marks_ext))
            # usn_name= "namehere"
            usn_college=u1.college
            usn_reg= u1.region
            usn_branch=u1.stream
            return render_template('result.html',name=usn_name,college=usn_college,region=usn_reg,branch=usn_branch,n=range(len(marks)), sub=subjects_vtu,extr=marks_ext,intr=marks_int,extpr=ext_sum_per,intpr=int_sum_per,tot=total_sub)
    return render_template('main.html', error=error)


@app.route('/last4',methods=['GET', 'POST'])
def last4():
    if request.method == 'POST':
        usn = request.form['usntest']
        student_last4 = Last4.student_VA()
        for i in range(5,9):
            student_last4.GetTheResults(usn,i)

        sem5sub,sem5ext,sem5int = student_last4.get_sem_list(5)
        sem5subs = [item.strip() for item in sem5sub if str(item)]
        sem5exts = [item.strip() for item in sem5ext if str(item)]
        sem5ints = [item.strip() for item in sem5int if str(item)]
        sem5ext = '[%s]' % ', '.join(map(str, sem5exts))
        sem5int = '[%s]' % ', '.join(map(str, sem5ints))

        sem6sub,sem6ext,sem6int = student_last4.get_sem_list(6)
        sem6subs = [item.strip() for item in sem6sub if str(item)]
        sem6exts = [item.strip() for item in sem6ext if str(item)]
        sem6ints = [item.strip() for item in sem6int if str(item)]
        sem6ext = '[%s]' % ', '.join(map(str, sem6exts))
        sem6int = '[%s]' % ', '.join(map(str, sem6ints))

        sem7sub,sem7ext,sem7int = student_last4.get_sem_list(7)
        sem7subs = [item.strip() for item in sem7sub if str(item)]
        sem7exts = [item.strip() for item in sem7ext if str(item)]
        sem7ints = [item.strip() for item in sem7int if str(item)]
        sem7ext = '[%s]' % ', '.join(map(str, sem7exts))
        sem7int = '[%s]' % ', '.join(map(str, sem7ints))

        sem8sub,sem8ext,sem8int = student_last4.get_sem_list(8)
        sem8subs = [item.strip() for item in sem8sub if str(item)]
        sem8exts = [item.strip() for item in sem8ext if str(item)]
        sem8ints = [item.strip() for item in sem8int if str(item)]
        sem8ext = '[%s]' % ', '.join(map(str, sem8exts))
        sem8int = '[%s]' % ', '.join(map(str, sem8ints))

        return render_template('last4.html',sem5sub=sem5subs,sem5ext=sem5ext,sem5int=sem5int,sem6sub=sem6subs,sem6ext=sem6ext,sem6int=sem6int,sem7sub=sem7subs,sem7ext=sem7ext,sem7int=sem7int,sem8sub=sem8subs,sem8ext=sem8ext,sem8int=sem8int)
    return """
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>TESTING</title>
  </head>
  <body>
    <form method="post">
      <input type="text" name="usntest" >
      <input type="submit">
    </form>
  </body>
</html>

    """

if __name__ == "__main__":
    app.run()
